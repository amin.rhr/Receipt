package com.example.amin.receipt;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import java.util.ArrayList;

import printer.PrintObject;
import printer.PrintObjectLocation;
import printer.PrintObjectType;
import printer.Printer;

public class MainActivity extends AppCompatActivity {

    private ArrayList<PrintObject> printObjectList;
    private ImageView receiptView;
    private Bitmap receiptBitmap;
    GlobalVariables globalVariables;
    Typeface typeface;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        receiptView = findViewById(R.id.receiptView);

        Printer.setContext(this);
        Printer.setWidth(384);
        globalVariables = new GlobalVariables();
        typeface = Typeface.createFromAsset(getAssets(),globalVariables.fontFile);
        Printer.setDefaultFont(typeface, 10);
       //Printer.setDefaultFont(Typeface.createFromAsset(getAssets(),globalVariables.fontFile), 5);
        printReceipt();
    }
    public void printReceipt(){
        printObjectList = new ArrayList<>();
        printObjectList.add(new PrintObject(PrintObjectLocation.Center, PrintObjectType.Image, R.drawable.shaparak).setScale(0.5));
        printObjectList.add(new PrintObject(PrintObjectLocation.Center, "فروشگاه هایپر").setExtra(0, false, false));
        printObjectList.add(new PrintObject());
        printObjectList.add(new PrintObject(PrintObjectLocation.Center, "تراکنش خرید").setExtra(0, false, false));
        printObjectList.add(new PrintObject());
        printObjectList.add(new PrintObject(PrintObjectLocation.Center, "رسید مشتری").setExtra(0, false, false));
        printObjectList.add(new PrintObject());
        printObjectList.add(new PrintObject(PrintObjectLocation.Right, "شماره پایانه").setExtra(2, false, false));
        printObjectList.add(new PrintObject(PrintObjectLocation.Left, "2062885").setExtra(2, false, false));
        printObjectList.add(new PrintObject());
        printObjectList.add(new PrintObject(PrintObjectLocation.Right, "شماره پذیرنده").setExtra(2, false, false));
        printObjectList.add(new PrintObject(PrintObjectLocation.Left, "4243654").setExtra(2, false, false));
        printObjectList.add(new PrintObject());
        printObjectList.add(new PrintObject(PrintObjectLocation.Right, "ملی").setExtra(2, false, false));
        printObjectList.add(new PrintObject(PrintObjectLocation.Left, "603799-***-1650").setExtra(2, false, false));
        printObjectList.add(new PrintObject());
        printObjectList.add(new PrintObject(PrintObjectLocation.Right, "شماره پیگیری").setExtra(2, false, false));
        printObjectList.add(new PrintObject(PrintObjectLocation.Left, "1").setExtra(2, false, false));
        printObjectList.add(new PrintObject());
        printObjectList.add(new PrintObject(PrintObjectLocation.Right, "زمان").setExtra(2, false, false));
        printObjectList.add(new PrintObject(PrintObjectLocation.Left, "1397/04/16 - 11:23:02").setExtra(2, false, false));
        printObjectList.add(new PrintObject());
        printObjectList.add(new PrintObject(PrintObjectLocation.Right, "شماره مرجع").setExtra(2, false, false));
        printObjectList.add(new PrintObject(PrintObjectLocation.Left, "12798347").setExtra(2, false, false));
        printObjectList.add(new PrintObject());
        printObjectList.add(new PrintObject(PrintObjectLocation.Center, "تراکنش با موفقیت انجام شد").setExtra(2, true, false));
        printObjectList.add(new PrintObject());
        printObjectList.add(new PrintObject(PrintObjectLocation.Right, "مبلغ").setExtra(2, false, true));
        printObjectList.add(new PrintObject(PrintObjectLocation.Left, "200000 ریال").setExtra(2, false, true));
        printObjectList.add(new PrintObject(PrintObjectType.Line));
        printObjectList.add(new PrintObject());
        printObjectList.add(new PrintObject(PrintObjectLocation.Center, PrintObjectType.Image , R.drawable.nab_logo).setScale(0.5));
        receiptBitmap = Printer.print(printObjectList);
        try {
            receiptView.setImageBitmap(receiptBitmap);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}
