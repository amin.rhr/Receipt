package com.example.amin.receipt;

import java.io.File;
import java.util.Random;

/**
 * Created by Hooman on 23/2/2018.
 */

public class GlobalVariables {
    public static final String fontFile = "BYekan.ttf";
    public static String privatePath;

    public static final String transactionsPath = "transactions";
    public static final String trnConfigFileName = "sequence.obj";
    public static final String terminalInfoFileName = "merchant.obj";

    public static File transactionsDir;
    public static File transactionConfigFile;
    public static File terminalInfoFile;

    public static final String timeFormat = "yyyy/MM/dd HH:mm:ss";
    public static final String numbers = "0123456789";
    public static int scannerTimeout = 120;

    public static String getSaltString(String set, int len) {
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < len) {
            salt.append(set.charAt((int) (rnd.nextFloat() * set.length())));
        }
        return salt.toString();
    }

}
