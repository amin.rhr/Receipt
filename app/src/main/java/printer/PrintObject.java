package printer;

import android.graphics.Typeface;

public class PrintObject {
    public PrintObjectLocation location;
    public PrintObjectType type;
    public String text;
    public Typeface font;
    public double scale = 1;
    public int resourceId = -1;
    public int fontSize = -1;
    public int padding = -1;
    public boolean border;
    public boolean bold;

    public PrintObject(PrintObjectLocation location, PrintObjectType type, int resourceId) {
        this.location = location;
        this.type = type;
        this.resourceId = resourceId;
    }

    public PrintObject(PrintObjectType type){
        this.type = type;
    }

    public PrintObject() {
        this.type = PrintObjectType.Break;
    }

    public PrintObject(PrintObjectLocation location, String text) {
        this.location = location;
        this.text = text;
        type = PrintObjectType.Text;
    }

    public PrintObject setExtra(int padding, boolean border, boolean bold) {
        this.padding = padding;
        this.border = border;
        this.bold = bold;
        return this;
    }

    public PrintObject setScale(double scale) {
        this.scale = scale;
        return this;
    }

    public PrintObject setFont(Typeface typeface, int size) {
        font = typeface;
        fontSize = size;
        return this;
    }
}