package printer;

public enum PrintObjectLocation {
    Right,
    Left,
    Center,
}
