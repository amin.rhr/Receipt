package printer;

public enum PrintObjectType {
    Text,
    Image,
    Break,
    Line,
}
