package printer;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.Log;

import java.util.List;

public class Printer {
    private static Context context;
    private static int defaultFontSize = -1;
    private static int width = -1;
    private static int centerX = -1;
    private static Typeface defaultFont;
    private static List<PrintObject> objects = null;
    private static final String TAG = "Printer";
    private static int padding;

    private Printer() {

    }

    public static void setWidth(int width) {
        Printer.width = width;
        Printer.centerX = width >> 1;
    }

    public static void setContext(Context context) {
        Printer.context = context;
    }

    public static void setDefaultFont(Typeface defaultFont) {
        Printer.defaultFont = defaultFont;
    }

    public static void setDefaultFontSize(int defaultFontSize) {
        Printer.defaultFontSize = defaultFontSize;
    }

    public static void setDefaultFont(Typeface defaultFont, int defaultFontSize) {
        Printer.defaultFontSize = defaultFontSize;
        Printer.defaultFont = defaultFont;
    }

    public static Bitmap getResizedBitmapByConstantAspectRatio(Bitmap bm, PrintObject imgObject) {
        int H = (int) ((bm.getHeight() * width) / bm.getWidth());
        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bm, (int) (width * imgObject.scale),
                (int) (H * imgObject.scale), false);
        bm.recycle();
        return resizedBitmap;
    }

    private static int getHeight(PrintObject obj) {
        switch (obj.type) {
            case Break:
                return 0;
            case Image:
                if (obj.resourceId == -1) {
                    throw new RuntimeException("No recourceID found");
                }
                Drawable d = context.getResources().getDrawable(obj.resourceId);
                return (int) (d.getIntrinsicHeight());
            case Line:
                return 2;
            case Text:
                if (obj.fontSize != -1) {
                    return (int) (obj.fontSize * obj.scale);
                } else {
                    return (int) (defaultFontSize * obj.scale) / 2;
                    // return 1;
                }
            default:
                return 0;
        }
    }

    private static boolean goesToNextLine(PrintObject last, PrintObject now) {
        if (last == now) {
            return true;
        } else if (last.type == PrintObjectType.Break) {
            return true;
        } else if (last.location == now.location) {
            return true;
        } else {
            return false;
        }
    }

    private static int predictHeight(List<PrintObject> printObjectList) {
        int h = 0, h1, h2, rows = 0;
        PrintObject lastObject = null;
        for (PrintObject i : printObjectList) {
            if (lastObject == null) {
                lastObject = i;
            }
            if (goesToNextLine(lastObject, i)) {
                h += getHeight(i);
                rows++;
            } else {
                h1 = getHeight(lastObject);
                h2 = getHeight(i);
                if (h1 != h2) {
                    h += Math.max(h1, h2);
                }
            }

            lastObject = i;
        }
        Log.d(TAG, "predictHeight: " + rows);
        return h + (rows * 2);
    }

    public static Bitmap print(List<PrintObject> printObjectList) {
        try {
            if (printObjectList == null || printObjectList.size() == 0) {
                throw new RuntimeException("No object to print");
            }
            int height = predictHeight(printObjectList);
            if (height == 0) {
                throw new RuntimeException("Height of the print elements is 0");
            }
            Bitmap bitmapResult = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmapResult);
            Paint paint = new Paint();
            paint.setColor(Color.WHITE);
            canvas.drawRect(0, 0, width, height, paint);
            paint.setTypeface(defaultFont);
            paint.setTextSize(defaultFontSize);
            int line = width / 12;
            int step = width / 20;
            int leftOffset = width / 30;
            int rightOffset = width - leftOffset;
            Paint borderPaint = new Paint();
            for (int i = 0; i < printObjectList.size(); i++) {
                paint.setTypeface(printObjectList.get(i).font);
                paint.setFakeBoldText(printObjectList.get(i).bold);
                paint.setColor(Color.BLACK);
                paint.setTextSize(printObjectList.get(i).fontSize);
                if (printObjectList.get(i).type == PrintObjectType.Image) {
                    Bitmap img = BitmapFactory.decodeResource(context.getResources(),
                            printObjectList.get(i).resourceId);
                    img = getResizedBitmapByConstantAspectRatio(img, printObjectList.get(i));
                    if (printObjectList.get(i).location == PrintObjectLocation.Center) {
                        int imgWidth = img.getWidth();
                        int imgOffset = (width - imgWidth) / 2;
                        canvas.drawBitmap(img, imgOffset, line, null);
                    }
                    if (printObjectList.get(i).location == PrintObjectLocation.Left) {
                        canvas.drawBitmap(img, leftOffset, line, null);
                    }
                    if (printObjectList.get(i).location == PrintObjectLocation.Right) {
                        int imgRightOffset = width - img.getWidth() - leftOffset;
                        canvas.drawBitmap(img, imgRightOffset, line, null);
                    }
                    line += img.getHeight() + (1.5 * step);
                }
                if (printObjectList.get(i).border) {
                    line += step / 2;
                    borderPaint.setColor(Color.BLACK);
                    borderPaint.setStyle(Paint.Style.STROKE);
                    borderPaint.setStrokeWidth(2);
                    Rect bounds1 = new Rect();
                    paint.getTextBounds(printObjectList.get(i).text, 0, printObjectList.get(i).text.length(), bounds1);
                    int textInBorderHeight = bounds1.height();
                    canvas.drawRect(leftOffset, line - textInBorderHeight, rightOffset, line + (textInBorderHeight / 2), borderPaint);
                }
                if (printObjectList.get(i).type == PrintObjectType.Text) {
                    if (printObjectList.get(i).location == PrintObjectLocation.Left) {
                        paint.setTextAlign(Paint.Align.LEFT);
                        if (printObjectList.get(i).padding != -1) {
                            padding = printObjectList.get(i).padding;
                            canvas.drawText(printObjectList.get(i).text, leftOffset + padding, line + padding, paint);
                        } else {
                            canvas.drawText(printObjectList.get(i).text, width, line, paint);
                        }
                    }
                    if (printObjectList.get(i).location == PrintObjectLocation.Center) {
                        paint.setTextAlign(Paint.Align.CENTER);
                        if (printObjectList.get(i).border) {
                            canvas.drawText(printObjectList.get(i).text, centerX, line, paint);
                            line += step / 2;
                        } else {
                            canvas.drawText(printObjectList.get(i).text, centerX, line, paint);
                        }
                    }
                    if (printObjectList.get(i).location == PrintObjectLocation.Right) {
                        paint.setTextAlign(Paint.Align.RIGHT);
                        if (printObjectList.get(i).padding != -1) {
                            padding = printObjectList.get(i).padding;
                            canvas.drawText(printObjectList.get(i).text, rightOffset - padding, line + padding, paint);
                        } else {
                            canvas.drawText(printObjectList.get(i).text, width
                                    , line, paint);
                        }
                    }
                }
                if (printObjectList.get(i).type == PrintObjectType.Line) {
                    line += step;
                    canvas.drawLine(
                            leftOffset, // startX
                            line, // startY
                            rightOffset, // stopX
                            line, // stopY
                            paint // Paint
                    );
                }
                if (printObjectList.get(i).type == PrintObjectType.Break) {
                    line += (1.2 * step);
                    if (printObjectList.get(i - 1).type == PrintObjectType.Text) {
                        Rect bounds = new Rect();
                        paint.getTextBounds(printObjectList.get(i - 1).text, 0, printObjectList.get(i - 1).text.length(), bounds);
                        int textHeight = bounds.height();
                        line += textHeight;
                    }
                }
            }
            return bitmapResult;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
